﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour
{
    public static int scoreRed;
    public static int scoreBlue;
    public Text text1;
    public Text text2;
    [HideInInspector] public int m_PlayerNumber;
    


    void Awake()
    {
        scoreRed = 0;
        scoreBlue = 0;

    }

    void Update()
    {
        Debug.Log(scoreRed + " " + scoreBlue);
        text1.text = "Score: " + scoreRed;
        text2.text = "Score: " + scoreBlue;
    }

    public static int HighScore()
    {
        return Mathf.Max(scoreRed, scoreBlue);
    }
}
