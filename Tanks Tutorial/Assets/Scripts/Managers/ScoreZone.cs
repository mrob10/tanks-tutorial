﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreZone : MonoBehaviour
{
    void OnTriggerStay(Collider other)
    {
        TankMovement temp = other.gameObject.GetComponent<TankMovement>();
        if(temp == null)
        {
            return;
        }

        if (temp.m_PlayerNumber == 1){
            ScoreManager.scoreRed += 1;     
        }
        if (temp.m_PlayerNumber == 2)
        {
            ScoreManager.scoreBlue += 1;
        }
    }
}
